package net.sf.okapi.simplifier.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLFileCompare;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripSimplifyXliff2TkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private XLIFF2Filter xliffFilter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	@Before
	public void setUp() throws Exception {
		xliffFilter = new XLIFF2Filter();
	}

	@After
	public void tearDown() throws Exception {
		xliffFilter.close();
	}
	
	@Test
	public void xliff2Files() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFiles("/xliff2/", Arrays.asList(".xliff", ".xlf"))) {
			runTest(true, file, "okf_xliff2", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs("/xliff2/"))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".xliff", ".xlf"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".simplify_xliff_extracted";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String merged = root + f + ".merged";
		
		List<String> locales = FileUtil.guessLanguages(file.getAbsolutePath());		
		LocaleId source = LocaleId.fromString(locales.get(0));
		LocaleId target = LocaleId.FRENCH;
		if (locales.size() >= 2) {
			target = LocaleId.fromString(locales.get(1));
		}
		
		RoundTripUtils.extract(source, target, original, xliff, configName, customConfigPath, segment, false);		
		RoundTripUtils.merge(source, target, false, original, xliff, merged, configName, customConfigPath);
		
		RoundTripUtils.extract(source, target, original, xliff, configName, customConfigPath, segment, true);		
		RoundTripUtils.merge(source, target, false, original, xliff, tkitMerged, configName, customConfigPath);
		
		XMLFileCompare compare = new XMLFileCompare();
		try {
			assertTrue("Compare XML: " + f, compare.compareFilesPerLines(merged, tkitMerged));
		} catch(Throwable e) {
			errCol.addError(e);
		}
	}
}
